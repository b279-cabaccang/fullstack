import Banner from "../components/Banner.js";
import Highlights from "../components/Highlights.js"
import HotProducts from "../components/HotProducts.js"

import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext.js";

import BackToTopButton from '../components/BackToTopButton.js';

export default function Home(){
	
	const { user } = useContext(UserContext);

	const data = {

	title: "Embark on Thrilling Journeys and Own the Ultimate PlayStation Collection",
	content: "Purchase Now and Level Up Your Gaming!",
	destination: "/products",
	label: "Shop Now!"
	}


	return(
			<>
				
				{localStorage.getItem("isAdmin") === "true" ? 
				(
				<Navigate to="/admin-dashboard" />
				) : (
				<div>
					<Highlights/>
					<Banner bannerProps={data} />
					<HotProducts />
					<BackToTopButton />
				</div>
			
				)}
			</>


		)
}