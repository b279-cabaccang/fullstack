import { useEffect, useState, useContext } from "react";
import { Card, Form, Container, Row, Col, CardGroup } from "react-bootstrap";
import { Navigate } from 'react-router-dom';
import UserContext from "../UserContext.js";
import ProductCard from "../components/ProductCard";
import styles from "../modules/styles.css"

import BackToTopButton from '../components/BackToTopButton.js';


export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [filterText, setFilterText] = useState("");
  const storedIsAdmin = localStorage.getItem('isAdmin');


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        const activeProducts = data.filter((product) => product.isActive);

        setProducts(activeProducts);
        setFilteredProducts(activeProducts);
      });
  }, []);

  const handleFilterChange = (event) => {
    const searchText = event.target.value.toLowerCase();
    setFilterText(searchText);

    const filtered = products.filter((product) =>
      product.name.toLowerCase().includes(searchText)
    );

    setFilteredProducts(filtered);
  };

 return (
   <>
     {storedIsAdmin === 'true' ? (
       <Navigate to="/admin-dashboard" />
     ) : (
       <>
         <h1 className="productHeader">Home > Store</h1>
         <Row className="my-3">
            <Col className="my-2">
               <Container className="mt-4">
                 <Row>
                   <Col md={6} className="mb-3">
                     <Form.Control
                       type="text"
                       placeholder="Filter by name"
                       value={filterText}
                       onChange={handleFilterChange}
                     />
                   </Col>
                 </Row>
                 <Row className="productRow">
                   {filteredProducts.map((product) => (
                      <Col key={product._id} sm={12} md={6} lg={3} className="mb-3">
                          
                            <ProductCard productProp={product} />
                          
                     </Col>
                   ))}
                 </Row>
                </Container>
             </Col>
         </Row>
       </>
     )}

     {
      <BackToTopButton />
     }
   </>
 );
}
