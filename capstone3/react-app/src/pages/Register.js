import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext.js";
import {useNavigate, Navigate} from "react-router-dom";
import Swal from 'sweetalert2';
import styles from "../modules/styles.css"




export default function Register(){
	const { user } = useContext(UserContext);

	//an object with methods to redirect the user
	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [fullAddress, setFullAddress] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		// Validation to enable register button
		if((email !== "" && mobileNo !== "" && fullAddress !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true);
		}
		else {
			setIsActive(false);
		}

	}, [email, mobileNo, fullAddress, password1, password2]);

	// Check if values are successfully binded
	console.log(email);
	console.log(mobileNo);
	console.log(fullAddress);
	console.log(password1);
	console.log(password2);

	function registerUser(e) {

	       // Prevents page redirection via form submission
	       e.preventDefault();

	       fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
	           method: "POST",
	           headers: {
	               'Content-Type': 'application/json'
	           },
	           body: JSON.stringify({
	               email: email
	           })
	       })
	       .then(res => res.json())
	       .then(data => {

	           console.log(data);

	           if(data === true){

	               Swal.fire({
	                   title: 'Duplicate email found',
	                   icon: 'error',
	                   text: 'Please provide a different email.'   
	               });

	           } else {

	               fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
	                   method: "POST",
	                   headers: {
	                       'Content-Type': 'application/json'
	                   },
	                   body: JSON.stringify({
	                   		email: email,
	                   		password: password1,
	                   		mobileNo: mobileNo,
	                   		fullAddress: fullAddress

	                   })
	               })
	               .then(res => res.json())
	               .then(data => {

	                   console.log(data);

	                   if(data === true){

	                       // Clear input fields
	                       setEmail('');
	                       setMobileNo('');
	                       setFullAddress('');
	                       setPassword1('');
	                       setPassword2('');

	                       Swal.fire({
	                           title: 'Registration successful',
	                           icon: 'success',
	                           text: 'Welcome to GameJunkie Digital PlayStation Games Store!'
	                       });

	                       // Allows us to redirect the user to the login page after registering for an account
	                       navigate("/login");

	                   } else {

	                       Swal.fire({
	                           title: 'Something wrong',
	                           icon: 'error',
	                           text: 'Please try again.'   
	                       });

	                   };

	               })
	           };

	       })

	   }

	


	
return(
	(user.token !== null) ?
		<Navigate to="/" />

	:

	<>	
	<h1 className="registerHeader">Home > Registration Page</h1>



	<div className="container-fluid my-5 registerFont" id="register">
		<div className="row">
			<div className="col-md-6" id="registerBackground">
			</div>
			<div className="col-md-6 px-5 py-5">


				<Form className="my-5" onSubmit={e => registerUser(e)}>
			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" onChange={e => setEmail(e.target.value)} value={email} required/>
			        <Form.Text className="text-muted" >
			          We'll never share your email with anyone else.
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="userMobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control type="text" placeholder="Enter mobile number" onChange={e => setMobileNo(e.target.value)} value={mobileNo} required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="userFullAddress">
			        <Form.Label>Full Address</Form.Label>
			        <Form.Control type="text" placeholder="Enter full address" onChange={e => setFullAddress(e.target.value)} value={fullAddress} required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Password" onChange={e => setPassword1(e.target.value)} value={password1} required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password2">
			        <Form.Label>Confirm Password</Form.Label>
			        <Form.Control type="password" placeholder="Confirm Password" onChange={e => setPassword2(e.target.value)} value={password2} required/>
			      </Form.Group>

			      {/*conditionally render submit button based on isActive state*/}
			      
			      { isActive ? 

			      <Button variant="primary" type="submit" id="submitBtn">
			        Register
			      </Button>

			      :

			      <Button variant="danger" type="submit" id="submitBtn" disabled>
			        Register
			      </Button>
			      }   
		    	</Form>


			</div>
    	</div>
    </div>







	</>
	
	)



	
}



{/*
<div class="container-fluid my-5" id="contact">
	<div class="row">
		<div class="col-md-6" id="contactBackground">
		</div>
		<div class="col-md-6 px-5 py-5">




			<h3 class="contact-title pt-5 my-3">CONTACT US</h3>
			<p class="contact-description mt-2 mb-3 mr-md-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
			<form class="mt-3">
			  <div class="form-group my-4">
			    <label for="exampleFormControlInput1" class="contact-label">Name</label>
			    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="John Doe">
			  </div>
			  <div class="form-group my-4">
			    <label for="exampleFormControlInput1" class="contact-label">Email address</label>
			    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="john.doe@email.com">
			  </div>
			  <div class="form-group my-4">
			    <label for="exampleFormControlTextarea1" class="contact-label">Message</label>
			    <textarea class="form-control" id="exampleFormControlTextarea1" rows="6"></textarea>
			  </div>
			  <div class="my-5 d-flex justify-content-end">
			  	<button id="form-btn" class="btn contact-button">Submit</button>
			  </div>
			</form>



		</div>
	</div>
</div>

*/}

