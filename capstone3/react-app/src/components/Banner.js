// Object destructuring
import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import styles from "../modules/styles.css"

export default function Banner({bannerProps}){
	console.log(bannerProps);

	const { title, content, destination, label } = bannerProps;

	return(
			<Row>
				<Col className="p-5 text-center">
					<h1 className="brandSlogan2">{title}</h1>
					<p>{content}</p>
					<Button as={Link} to={destination} variant="primary">{label}</Button>
				</Col>
			</Row>

		);
}