import { Card, Button, Form, Modal, Image } from "react-bootstrap"
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom"
import { Link } from "react-router-dom"
import Table from 'react-bootstrap/Table';
import Styles from "../modules/styles.css"
import Swal from "sweetalert2"

import { useMediaQuery } from 'react-responsive';





export default function AdminDashboardTable({productProp}){
	// Checks if props was successfully passed
	console.log(productProp.name);
	// Checks the type of the passed data
	console.log(typeof productProp);


	// Destructuring the courseProp into their own variables
	const { _id, name, description, price, isActive, image } = productProp;
	const { productId } = useParams();


	const [archive, setIsArchive] = useState("")
	const [unarchive, setIsUnarchive] = useState("")

	const [showModal, setShowModal] = useState(false);
	const [modalName, setModalName] = useState(name);
	const [modalDescription, setModalDescription] = useState(description);
	const [modalPrice, setModalPrice] = useState(price);
	const [modalIsActive, setModalIsActive] = useState(isActive);
	const [modalImage, setModalImage] = useState(image);

	




		// Update AND Delete (Archive/Unarchive) product
		
		const handleProductUpdate = (e) => {
			e.preventDefault();


			fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
			    method: "PUT",
			    headers: {
			        'Content-Type': 'application/json',
			        Authorization: `Bearer ${localStorage.getItem("token")}`
			    },
			    body: JSON.stringify({
			        name: modalName,
			        description: modalDescription,
			        price: modalPrice,
			        isActive: modalIsActive,
			        image: modalImage
			    }),
			})
			.then(res => res.json())
			.then(data => {

			    console.log(data);

			            if(data === true){

			                // Clear input fields
			                setModalName('');
			                setModalDescription('');
			                setModalPrice('');
			                setModalIsActive(false);
			                setModalImage('');



			                setShowModal(false);

			                Swal.fire({
			                    title: 'Product Updated',
			                    icon: 'success',
			                    text: 'You have updated the product',
			                }).then(() => {
		                	window.location.reload();
		                })
			                setShowModal(true)

			            } else {

			                Swal.fire({
			                    title: 'Something wrong',
			                    icon: 'error',
			                    text: 'Please try again.'   
			                });

			            };

			        })
					

			    };

			    const handleUpdateProduct = () => {
			    	setModalIsActive(isActive);
			    	setShowModal(true)
			    };


	return (

			<>
			<Table striped bordered hover responsive>
			    <tbody>
			        <tr>
			          <td className="tableCell1">{name}</td>
			          <td>{description}</td>
			          <td className="tableCell1">PhP {price}</td>
			          <td className="tableCell1">productActive? "{isActive.toString()}"</td>
			          <td className="tableCell1">
			          	<Button variant="success" className="my-1" type="button" id="submitBtn" onClick={handleUpdateProduct}>
		          		  Update
		          		</Button>
			          </td>
			        </tr>
			    </tbody>
			  </Table>

			  <Modal show={showModal} onHide={() => setShowModal(false)} className="modal-dashboard" contentClassName="modal-content-dashboard">
			    <Modal.Header closeButton>
			      <Modal.Title>Update Product</Modal.Title>
			    </Modal.Header>
			    <Modal.Body>
			      <Form>
			        <Form.Group controlId="name">
			          <Form.Label>Name</Form.Label>
			          <Form.Control
			            type="text"
			            placeholder="Enter product name"
			            value={modalName}
			            onChange={(e) => setModalName(e.target.value)}
			            required
			          />
			        </Form.Group>
			        <Form.Group controlId="description">
			          <Form.Label>Description</Form.Label>
			          <Form.Control
			            as="textarea"
			            rows={6}
			            placeholder="Enter product description"
			            value={modalDescription}
			            onChange={(e) => setModalDescription(e.target.value)}
			            required
			          />
			        </Form.Group>
			        <Form.Group controlId="price">
			          <Form.Label>Price</Form.Label>
			          <Form.Control
			            type="number"
			            placeholder="Enter product price"
			            value={modalPrice}
			            onChange={(e) => setModalPrice(e.target.value)}
			            required
			          />
			        </Form.Group>

			        <Form.Group controlId="isActive">
			               <Form.Check
			                 type="checkbox"
			                 label="Active"
			                 checked={modalIsActive}
			                 onChange={(e) => setModalIsActive(e.target.checked)}
			               />
			        </Form.Group>

			        <Form.Group controlId="image">
			            <Form.Label>Image Link</Form.Label>
			            <Form.Control
			                type="text"
			                placeholder="Enter Product Image Link"
			                value={modalImage}
			                onChange={e => setModalImage(e.target.value)}
			            />
			        </Form.Group>
			        

			        <Modal.Footer>
			          <Button variant="secondary" onClick={() => setShowModal(false)}>
			            Close
			          </Button>
			          <Button variant="primary" onClick={handleProductUpdate}>
			            Update Product
			          </Button>
			        </Modal.Footer>
			      </Form>
			    </Modal.Body>
			  </Modal>
			</>

		  );
}

