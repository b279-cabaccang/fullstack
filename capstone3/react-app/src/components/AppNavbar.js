import React from 'react';
import {Nav, Navbar} from "react-bootstrap"
import { Link, NavLink } from "react-router-dom";
import { Container } from "react-bootstrap"
import { useState, useContext, useEffect } from "react"
import UserContext from "../UserContext"
import styles from "../modules/styles.css"

export default function AppNavbar(){

  // State to store user info stored in the login page
  /*const [user, setUser] = useState(localStorage.getItem("email"));
  console.log(user);*/

  const { user } = useContext(UserContext);


  console.log('user.token:', user.token);
   console.log('user.isAdmin:', user.isAdmin);

   console.log(user)

   const storedToken = localStorage.getItem('token');
   const storedIsAdmin = localStorage.getItem('isAdmin');
   const storedEmail = localStorage.getItem('email')
   
	return(

    <Navbar expand="lg" className="px-3 navbar_bg">
      <Navbar.Brand as={Link} to={"/"} className="navBarBrand">
      <img
      alt=""
      src={process.env.PUBLIC_URL + './images/controller.png'}
      width="30"
      height="30"
      className="d-inline-block align-top rounded image_rotate"
      />{' '}
        GameJunkie
        <div className="brandSlogan">Unlock Limitless Gaming with Digital PlayStation Delights.</div>
      </Navbar.Brand>

      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto pe-5">

        {
          storedToken && storedIsAdmin === "true" ? (
                    <>
                      <span className="mt-2 status-icon online"></span>
                      <span className="ps-1 pe-5 loggedInAs">Logged in as: {storedEmail} [ADMIN]</span>
                      <Nav.Link as={NavLink} to={"/admin-dashboard"} className="navLink ps-4 pe-3">Dashboard</Nav.Link>
                      <Nav.Link as={NavLink} to={"/logout"} className="navLink ps-4 pe-3">Logout</Nav.Link>
                    </>
                  ) : storedToken ? (
                    <>
                      <span className="mt-2 status-icon online"></span>
                      <span className="ps-1 pe-5 loggedInAs">Logged in as: {storedEmail}</span>
                      <Nav.Link as={NavLink} to={"/"} className="navLink ps-4 pe-3">Home</Nav.Link>
                      <Nav.Link as={NavLink} to={"/products"} className="navLink ps-4 pe-3">Products</Nav.Link>
                      <Nav.Link as={NavLink} to={"/logout"} className="navLink ps-4 pe-3">Logout</Nav.Link>
                    </>
                  ) : (
                    <>
                      <span className="mt-2 status-icon offline"></span>
                      <span className="ps-1 pe-5 loggedInAs">Currently not logged in.</span>
                      <Nav.Link as={NavLink} to={"/"} className="navLink ps-4 pe-3">Home</Nav.Link>
                      <Nav.Link as={NavLink} to={"/products"} className="navLink ps-4 pe-3">Store</Nav.Link>
                      <Nav.Link as={NavLink} to={"/register"} className="navLink ps-4 pe-3">Register</Nav.Link>
                      <Nav.Link as={NavLink} to={"/login"} className="navLink ps-4 pe-3">Login</Nav.Link>
                    </>
                  )
         
        }


        </Nav>
      </Navbar.Collapse>
    </Navbar>
		)
}

