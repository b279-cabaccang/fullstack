import { Card, Button, Row, Col } from "react-bootstrap"
import { useState } from "react";
import { Link } from "react-router-dom"
import styles from "../modules/styles.css"


export default function ProductCard({productProp}){
	// Checks if props was successfully passed
	console.log(productProp.name);
	// Checks the type of the passed data
	console.log(typeof productProp);

	// Destructuring the courseProp into their own variables
	const { _id, name, description, price } = productProp;

	return(
		<Row>
			<Col>
				<Card>
					<Card.Body className="my-3 productCard">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Link className="btn btn-primary" to={`/productView/${_id}`}>Details</Link>
					</Card.Body>
				</Card>

			</Col>

		</Row>



		)
}

