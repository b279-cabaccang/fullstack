import { Row, Col, Card } from "react-bootstrap";
import { Carousel } from "react-bootstrap";


export default function Highlights(){
	return(

	<div className="carousel-container mt-4">
		<Carousel controls={false} interval={2000}>
		      <Carousel.Item>
		      	<div className="carousel-overlay">
		      		<div className="carousel-overlay-inner">
		      			<p>Purchase PlayStation games across all generations</p>
		      		</div>
		      	</div>
		        <img
		          className="carousel-image img-fluid"
		          src="./images/playstationWall.jpg"
		          alt="First slide"
		        />
		      </Carousel.Item>
		      <Carousel.Item>
		      	<div className="carousel-overlay">
		      		<div className="carousel-overlay-inner">
		      			<p>Rediscover the Legends. Embrace the Classics. Dive into the Extraordinary World of Classic PlayStation Games.</p>
		      		</div>
		      	</div>
		        <img
		          className="carousel-image img-fluid"
		          src="./images/ps2banner.jpg"
		          alt="First slide"
		        />
		      </Carousel.Item>
		      <Carousel.Item>
		      	<div className="carousel-overlay">
		      		<div className="carousel-overlay-inner">
		      			<p>Elevate Your Gaming Experience. Unleash the Power of PlayStation. Enter a New Era of Gaming Excellence.</p>
		      		</div>
		      	</div>
		        <img
		          className="carousel-image img-fluid"
		          src="./images/ps4banner.jpg"
		          alt="First slide"
		        />
		      </Carousel.Item>
		    </Carousel>
	</div>


		)

}