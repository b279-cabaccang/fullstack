import { Card, Button, Form, Modal } from "react-bootstrap"
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom"
import { Link } from "react-router-dom"
import Table from 'react-bootstrap/Table';
import Styles from "../modules/styles.css"
import Swal from "sweetalert2"




export default function AdminDashboardTable({productProp}){
	// Checks if props was successfully passed
	console.log(productProp.name);
	// Checks the type of the passed data
	console.log(typeof productProp);


	// Destructuring the courseProp into their own variables
	const { _id, name, description, price, isActive } = productProp;
	const { productId } = useParams();


	const [archive, setIsArchive] = useState("")
	const [unarchive, setIsUnarchive] = useState("")

	const [showModal, setShowModal] = useState(false);
	const [modalName, setModalName] = useState("");
	const [modalDescription, setModalDescription] = useState("");
	const [modalPrice, setModalPrice] = useState("");


	// Archive product 
	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setIsArchive(data.isActive);
		})
	}, [productId])

	// Unarchive product 
	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setIsArchive(data.isActive);
		})
	}, [productId])


		// Archive product 
		const archiveProduct = (productId) => {
		  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
		    method: "PUT",
		    headers: {
		      "Content-type": "application/json",
		      Authorization: `Bearer ${localStorage.getItem("token")}`
		    },
		    body: JSON.stringify({
		      isActive: false
		    })
		  })
		    .then(res => res.json())
		    .then(data => {
		      console.log(data);

		      if (data === true) {
		        Swal.fire({
		          title: "Successfully archived",
		          icon: 'success',
		          text: "You have successfully archived this product"
		        });
		      } else {
		        Swal.fire({
		          title: "Archive failed",
		          icon: 'error',
		          text: "Failed to archive this product"
		        });
		      }
		    });
		}


		// Unrchive product 
		const unarchiveProduct = (productId) => {
		  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/unarchive`, {
		    method: "PUT",
		    headers: {
		      "Content-type": "application/json",
		      Authorization: `Bearer ${localStorage.getItem("token")}`
		    },
		    body: JSON.stringify({
		      isActive: true
		    })
		  })
		    .then(res => res.json())
		    .then(data => {
		      console.log(data);

		      if (data === true) {
		        Swal.fire({
		          title: "Successfully unarchived",
		          icon: 'success',
		          text: "You have successfully unarchived this product"
		        });
		      } else {
		        Swal.fire({
		          title: "Unarchive failed",
		          icon: 'error',
		          text: "Failed to unarchive this product"
		        });
		      }
		    });
		}


		// Update product
		
		const handleProductUpdate = (e) => {
			e.preventDefault();


			fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
			    method: "PUT",
			    headers: {
			        'Content-Type': 'application/json',
			        Authorization: `Bearer ${localStorage.getItem("token")}`
			    },
			    body: JSON.stringify({
			        name: modalName,
			        description: modalDescription,
			        price: modalPrice,
			    }),
			})
			.then(res => res.json())
			.then(data => {

			    console.log(data);

			            if(data === true){

			                // Clear input fields
			                setModalName('');
			                setModalDescription('');
			                setModalPrice('');

			                setShowModal(false);

			                Swal.fire({
			                    title: 'Product Updated',
			                    icon: 'success',
			                    text: 'You have updated the product',
			                }).then(() => {
		                	window.location.reload();
		                })
			                setShowModal(true)

			            } else {

			                Swal.fire({
			                    title: 'Something wrong',
			                    icon: 'error',
			                    text: 'Please try again.'   
			                });

			            };

			        })
					

			    };

			    const handleUpdateProduct = () => {
			    	setShowModal(true)
			    };



	return (

		<>
		<Table striped bordered hover>
		    <tbody>
		        <tr>
		          <td className="tableCell">{name}</td>
		          <td>{description}</td>
		          <td className="tableCell">PhP {price}</td>
		          <td className="tableCell">productActive? "{isActive.toString()}"</td>
		          <td> 
	          	
	          		<Button variant="success" className="my-1" type="button" id="submitBtn" onClick={handleUpdateProduct}>
	          		  Update
	          		</Button>
		          	



		          	<Modal show={showModal} onHide={() => setShowModal(false)}>
		          	  <Modal.Header closeButton>
		          	    <Modal.Title>Update Product</Modal.Title>
		          	  </Modal.Header>
		          	  <Modal.Body>
		          	    <Form>
		          	      <Form.Group controlId="name">
		          	        <Form.Label>Name</Form.Label>
		          	        <Form.Control
		          	          type="text"
		          	          placeholder="Enter product name"
		          	          value={modalName}
		          	          onChange={(e) => setModalName(e.target.value)}
		          	          required
		          	        />
		          	      </Form.Group>
		          	      <Form.Group controlId="description">
		          	        <Form.Label>Description</Form.Label>
		          	        <Form.Control
		          	          type="text"
		          	          placeholder="Enter product description"
		          	          value={modalDescription}
		          	          onChange={(e) => setModalDescription(e.target.value)}
		          	          required
		          	        />
		          	      </Form.Group>
		          	      <Form.Group controlId="price">
		          	        <Form.Label>Price</Form.Label>
		          	        <Form.Control
		          	          type="number"
		          	          placeholder="Enter product price"
		          	          value={modalPrice}
		          	          onChange={(e) => setModalPrice(e.target.value)}
		          	          required
		          	        />
		          	      </Form.Group>
		          	      

		          	      <Modal.Footer>
		          	        <Button variant="secondary" onClick={() => setShowModal(false)}>
		          	          Close
		          	        </Button>
		          	        <Button variant="primary" onClick={handleProductUpdate}>
		          	          Update Product
		          	        </Button>
		          	      </Modal.Footer>
		          	    </Form>
		          	  </Modal.Body>
		          	</Modal>






		          	<form>
		          		<Button variant="danger" className="my-1" type="submit" id="submitBtn" onClick={() => archiveProduct(_id)}>
		          		  Archive
		          		</Button>
		          	</form>
		          	<form>
		          		<Button variant="primary" className="my-1" type="submit" id="submitBtn" onClick={() => unarchiveProduct(_id)}>
		          		  Unarchive
		          		</Button>
		          	</form>

		          </td>
		        </tr>
		    </tbody>
		  </Table>
		</>

	  );
}









router.post("/createProduct", (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	
});

// Update product information
router.put("/:productId", (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports.addProduct = (reqBody) => {
			let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		})

			// Saves the created object to our DB(MongoDB)
			return newProduct.save().then((product, error) => {
				if(error){
					return false;
				}
				return true;

			})
}



// Update product information
module.exports.updateProduct = (reqParams, reqBody) => {
			let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})



	// Archive product (Admin only)
router.put("/:id/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	const productId = req.params.id;
	productController.archiveProduct(productId, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Unarchive product (Admin only)
router.put("/:id/unarchive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	const productId = req.params.id;
	productController.unarchiveProduct(productId, isAdmin).then(resultFromController => res.send(resultFromController));
})


// Archive product (Admin only)
module.exports.archiveProduct = (productId, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
			let archivedProduct = {
			isActive: false
		}

		return Product.findByIdAndUpdate(productId, archivedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}

// Unarchive product (Admin only)
module.exports.unarchiveProduct = (productId, isAdmin) => {

	if(isAdmin){
			let unarchivedProduct = {
			isActive: true
		}

		return Product.findByIdAndUpdate(productId, unarchivedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}



/*

	// Archive product 
	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setIsArchive(data.isActive);
		})
	}, [productId])

	// Unarchive product 
	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setIsArchive(data.isActive);
		})
	}, [productId])


		// Archive product 
		const archiveProduct = (productId) => {
		  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
		    method: "PUT",
		    headers: {
		      "Content-type": "application/json",
		      Authorization: `Bearer ${localStorage.getItem("token")}`
		    },
		    body: JSON.stringify({
		      isActive: false
		    })
		  })
		    .then(res => res.json())
		    .then(data => {
		      console.log(data);

		      if (data === true) {
		        Swal.fire({
		          title: "Successfully archived",
		          icon: 'success',
		          text: "You have successfully archived this product"
		        });
		      } else {
		        Swal.fire({
		          title: "Archive failed",
		          icon: 'error',
		          text: "Failed to archive this product"
		        });
		      }
		    });
		}


		// Unrchive product 
		const unarchiveProduct = (productId) => {
		  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/unarchive`, {
		    method: "PUT",
		    headers: {
		      "Content-type": "application/json",
		      Authorization: `Bearer ${localStorage.getItem("token")}`
		    },
		    body: JSON.stringify({
		      isActive: true
		    })
		  })
		    .then(res => res.json())
		    .then(data => {
		      console.log(data);

		      if (data === true) {
		        Swal.fire({
		          title: "Successfully unarchived",
		          icon: 'success',
		          text: "You have successfully unarchived this product"
		        });
		      } else {
		        Swal.fire({
		          title: "Unarchive failed",
		          icon: 'error',
		          text: "Failed to unarchive this product"
		        });
		      }
		    });
		}
*/

{/*
			          	<form>
			          		<Button variant="danger" className="my-1" type="submit" id="submitBtn" onClick={() => archiveProduct(_id)}>
			          		  Archive
			          		</Button>
			          	</form>
			          	<form>
			          		<Button variant="primary" className="my-1" type="submit" id="submitBtn" onClick={() => unarchiveProduct(_id)}>
			          		  Unarchive
			          		</Button>
			          	</form>

*/}



// WORKING PRODUCT.JS (THIS IS THE ORIGINAL!! WAAAHH!!)



import productData from "../data/products"
import ProductCard from '../components/ProductCard'
import { useEffect, useState, useContext} from "react"
import UserContext from "../UserContext.js";
import { Navigate } from 'react-router-dom';
import styles from "../modules/styles.css";


export default function Products(){
	// Checks to see if mock data was captured
/*	console.log(courseData);
	console.log(courseData[0]);
*/
	// State that will be used to store courses retrieved from db

	const { user } = useContext(UserContext);


	const [AllProducts, setAllProducts] = useState([]);
	const storedToken = localStorage.getItem('token');
	const storedIsAdmin = localStorage.getItem('isAdmin');

	// Retrieves the courses from database upon initial render of the "Courses" Component

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			const activeProducts = data.filter((product) => product.isActive);

			setAllProducts(
				activeProducts.map((product) => {
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))

		})
	}, [])


	return (
	  <>
	    {user.isAdmin ? (
	      <Navigate to="/admin-dashboard" />
	    ) : (
	      <>
	        <h1 className="productHeader">Home > Store</h1>
	        <Container className="mt-4">
	          <Row>
	            <Col md={6} className="mb-3">
	              <Form.Control
	                type="text"
	                placeholder="Filter by name"
	                value={filterText}
	                onChange={handleFilterChange}
	              />
	            </Col>
	          </Row>
	          <Row className="productRow">
	            {filteredProducts.map((product) => (
	              <Col key={product._id} sm={12} md={5} lg={3} className="mb-3 mx-1">
	                <ProductCard productProp={product} />
	              </Col>
	            ))}
	          </Row>
	        </Container>
	      </>
	    )}
	  </>
	);