import { Row, Col, Card } from "react-bootstrap";

export default function HotProducts(){

	return(
		<Row className="my-3">
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/hoticon.png'}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        />{' Hot PlayStation 1 Game'}
                            <h2>Silent Hill</h2>
                        </Card.Title>
                    </Card.Header>
                    <div className="mx-auto">
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/silenthill1.jpg'}
                        width="100%"
                        height="80%"
                        className="d-inline-block align-top"
                        />
                    </div>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/hoticon.png'}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        />{' Hot PlayStation 2 Game'}
                            <h4>Metal Gear Solid 3: Snake Eater</h4>
                        </Card.Title>
                    </Card.Header>
                    <div className="mx-auto">
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/mgs3.jpg'}
                        width="100%"
                        height="60%"
                        className="d-inline-block align-top"
                        />
                    </div>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/hoticon.png'}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        />{' Hot PlayStation 3 Game'}
                            <h2>The Last of Us</h2>
                        </Card.Title>
                    </Card.Header>
                    <div className="mx-auto">
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/tlou.jpg'}
                        width="100%"
                        height="80%"
                        className="d-inline-block align-top"
                        />
                    </div>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/hoticon.png'}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        />{' Hot PlayStation 4 Game'}
                            <h2>The Last of Us Part II</h2>
                        </Card.Title>
                    </Card.Header>
                    <div className="mx-auto">
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/tlou2.jpg'}
                        width="100%"
                        height="80%"
                        className="d-inline-block align-top"
                        />
                    </div>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/hoticon.png'}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        />{' Hot PlayStation 5 Game'}
                            <h2>God of War: Ragnarok</h2>
                        </Card.Title>
                    </Card.Header>
                    <div className="mx-auto">
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/gow.webp'}
                        width="100%"
                        height="100%"
                        className="d-inline-block align-top"
                        />
                    </div>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/hoticon.png'}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        />{' Hot PSP/Vita Game'}
                            <h3>Uncharted Golden Abyss</h3>
                        </Card.Title>
                    </Card.Header>
                    <div className="mx-auto">
                        <img
                        alt=""
                        src={process.env.PUBLIC_URL +'./images/uncharted.jpg'}
                        width="100%"
                        height="80%"
                        className="d-inline-block align-top"
                        />
                    </div>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
	)
}