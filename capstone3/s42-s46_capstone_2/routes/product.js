const express = require("express");
const router = express.Router();
const productController = require("../controllers/product.js");
const auth = require("../auth.js");




// Create Product (Admin only)
/*
router.post("/createProduct", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	
});

*/
router.post("/createProduct", (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	
});


router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})


/*
// Retrieve all products (Admin only)
router.get("/all", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	productController.getAllProducts(data.isAdmin).then(resultFromController => res.send(resultFromController));
})

// Retrieve all active products
router.get("/allProducts", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})
*/



// Retrieve single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Update product information
router.put("/:productId", (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


// Archive product (Admin only)
router.put("/:id/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	const productId = req.params.id;
	productController.archiveProduct(productId, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Unarchive product (Admin only)
router.put("/:id/unarchive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	const productId = req.params.id;
	productController.unarchiveProduct(productId, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Archive product (Admin only)
module.exports.archiveProduct = (productId, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
			let archivedProduct = {
			isActive: false
		}

		return Product.findByIdAndUpdate(productId, archivedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}

// Unarchive product (Admin only)
module.exports.unarchiveProduct = (productId, isAdmin) => {

	if(isAdmin){
			let unarchivedProduct = {
			isActive: true
		}

		return Product.findByIdAndUpdate(productId, unarchivedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}









// Allows us to export the "router" object that will be accessed in our "index.js" file

module.exports = router;