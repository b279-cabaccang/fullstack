const Product = require("../models/Product.js");

// Create Product (Admin only)
/*module.exports.addProduct = (data) => {
		console.log(data.isAdmin);
			if (data.isAdmin){
				let newProduct = new Product({
				name: data.product.name,
				description: data.product.description,
				price: data.product.price,
				isActive: data.product.isActive
			})

			// Saves the created object to our DB(MongoDB)
			return newProduct.save().then((course, error) => {
				if(error){
					return false;
				}
				return true;

			})
		}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

*/

module.exports.addProduct = (reqBody) => {
			let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		})

			// Saves the created object to our DB(MongoDB)
			return newProduct.save().then((product, error) => {
				if(error){
					return false;
				}
				return true;

			})
}


// retrieve all products function
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

/*
// Retrieve all products (Admin only)
module.exports.getAllProducts = (isAdmin) => {
		if(isAdmin){
		return Product.find({}).then(result => {
			return result;
		});
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value;
	})
}

// Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({}).then(result => {
		return result;
	})
}
*/




// Retrieve single product
module.exports.getCourse = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update product information
module.exports.updateProduct = (reqParams, reqBody) => {
			let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			image: reqBody.image,
			isActive: reqBody.isActive
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})


// Archive product (Admin only)
module.exports.archiveProduct = (productId, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
			let archivedProduct = {
			isActive: false
		}

		return Product.findByIdAndUpdate(productId, archivedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}

// Unarchive product (Admin only)
module.exports.unarchiveProduct = (productId, isAdmin) => {

	if(isAdmin){
			let unarchivedProduct = {
			isActive: true
		}

		return Product.findByIdAndUpdate(productId, unarchivedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}