const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "NAME IS REQUIRED!"]
	},
	description: {
		type: String,
		required: [true, "DESCRIPTION IS REQUIRED!"]
	},
	image: {
		type: String
	},
	price: {
		type: Number,
		required: [true, "PRICE IS REQUIRED!"]
	},
	quantity: {
		type: Number
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [
			{
				userId: {
					type: String,
					default: ""
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}

		]
	
})


module.exports = mongoose.model("Product", productSchema);