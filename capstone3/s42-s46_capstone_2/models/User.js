const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "EMAIL IS REQUIRED!"]
	},
	password: {
		type: String,
		required: [true, "PASSWORD IS REQUIRED!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "MOBILE NUMBER IS REQUIRED!"]
	},

	fullAddress: {
		type: String,
		required: [true, "FULL ADDRESS IS REQUIRED!"]
	},

	orderedProduct: [
			{
				products: [
					{
						productId: {
							type: mongoose.Schema.Types.ObjectId
						},
						productName: {
							type: String,
						},
						description: {
							type: String,
						},
						price: {
							type: Number
						},
						quantity: {
							type: Number
						}
					}
				],

				totalAmount: {
					type: Number
				},
				subtotal: {
					type: Number
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}

		]
	
})


module.exports = mongoose.model("User", userSchema);